package com.exam.viapath.domain.models

import com.google.gson.annotations.SerializedName

data class DetailReceiptResponse(
    @SerializedName("title") val title: String?,
    @SerializedName("creditsText") val creditsText: String?,
    @SerializedName("sourceUrl") val urlWebSite: String?,
    @SerializedName("summary") val summary: String?,
    @SerializedName("readyInMinutes") val time: Int?,
    @SerializedName("image") val image: String?,
    @SerializedName("instructions") val instructions: String?,
    @SerializedName("extendedIngredients") val extendedIngredients: List<ExtendedIngredients>?,
    @SerializedName("pricePerServing") val pricePerServing: Float?,

) {
    data class ExtendedIngredients(
        @SerializedName("aisle") val aisle: String?,
        @SerializedName("nameClean") val nameClean: String?,
        @SerializedName("amount") val amount: Float?,
        @SerializedName("unit") val unit: String?,
        @SerializedName("original") val original: String?
    )
}
