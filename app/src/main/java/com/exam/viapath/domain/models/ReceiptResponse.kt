package com.exam.viapath.domain.models

import com.google.gson.annotations.SerializedName

data class ReceiptResponse(
    @SerializedName("results") val listResult: List<Receipt>?
) {
    data class Receipt(
        @SerializedName("image") val image: String?,
        @SerializedName("id") val id: Int?,
        @SerializedName("title") val title: String?
    )
}