package com.exam.viapath.di.module

import android.app.Application
import android.content.Context
import com.exam.viapath.data.database.MyDataBaseHelper
import com.exam.viapath.di.application.InjectableApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object MyExamAppModule {

    @Provides
    @Singleton
    @JvmStatic
    fun providesContext(app: InjectableApplication) : Context = app.applicationContext

    @Provides
    @Singleton
    @JvmStatic
    fun providesApplication(app: InjectableApplication) : Application = app

    @Provides
    @Singleton
    fun providesDataBase(context: Context): MyDataBaseHelper = MyDataBaseHelper(context)
}