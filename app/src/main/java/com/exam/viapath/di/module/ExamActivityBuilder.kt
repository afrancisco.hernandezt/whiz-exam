package com.exam.viapath.di.module

import com.exam.viapath.scope.ActivityScope
import com.exam.viapath.ui.activity.ReceiptActivity
import com.exam.viapath.ui.module.SearchReceiptModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ExamActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            SearchReceiptModule::class
        ]
    )
    abstract fun bindReceiptActivity(): ReceiptActivity
}