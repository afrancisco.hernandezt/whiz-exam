package com.exam.viapath.di.application

import com.exam.viapath.di.component.DaggerMainComponent
import com.exam.viapath.di.component.MainComponent
import com.exam.viapath.di.providers.ComponentHolder
import com.exam.viapath.di.providers.MyExamComponentProvider

class MainApplication : MyExamApplication(), MyExamComponentProvider {

    private fun getMainComponent() = component as MainComponent

    override fun provideAppComponent() = getMainComponent()

    override fun initializeInjector() {
        val component = DaggerMainComponent.builder()
            .application(this)
            .build()
            .apply {
                inject(this@MainApplication)
            }
        this.component = component

        ComponentHolder.clear()
        ComponentHolder.addOrReplace(component)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: MainApplication
            private set
    }
}