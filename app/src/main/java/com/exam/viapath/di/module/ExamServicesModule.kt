package com.exam.viapath.di.module

import com.exam.viapath.di.rest.Microservices
import com.exam.viapath.network.retrofitBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(
    includes = [
        ExamServicesApiModule::class
    ]
)
object ExamServicesModule {

    @Provides
    @Singleton
    @Microservices
    @JvmStatic
    fun microServicesHttpClient(
        httpClient: OkHttpClient
    ): Retrofit {
        return retrofitBuilder(
            httpClient,
            "https://api.spoonacular.com/"
        ).build()
    }
}