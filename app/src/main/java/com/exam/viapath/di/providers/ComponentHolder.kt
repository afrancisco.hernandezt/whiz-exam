package com.exam.viapath.di.providers

object ComponentHolder {

    /**
     * List that holds the components that want be shareable a cross the application.
     * The main case is the ApplicationComponent.
     */
    @PublishedApi
    internal var components = mutableListOf<Any>()

    /**
     * Adds a [component] if it does not exist in the list.
     * If the [component] does exist, it replaces the existing component.
     */
    inline fun <reified T : Any> addOrReplace(component: T) {
        synchronized(this) {
            val indexOfExistingComponent = components.indexOfFirst { it is T }
            if (indexOfExistingComponent != -1) {
                components[indexOfExistingComponent] = component
            } else {
                components.add(component)
            }
        }
    }

    /**
     * Indicates if a given component with type [T] exists in the list.
     */
    inline fun <reified T : Any> exists(): Boolean {
        synchronized(this) {
            return components.find { it is T } != null
        }
    }

    /**
     * Removes all components from the list that conform to the type [T].
     */
    inline fun <reified T : Any> remove() {
        synchronized(this) {
            components.removeAll { it is T }
        }
    }

    /**
     * Clears the list of components and assigns a new and clean instance to it.
     */
    fun clear() {
        components.clear()
        components = mutableListOf()
    }

    inline fun <reified T : Any> clearAndAddComponent(component: T) {
        synchronized(this) {
            components.clear()
            components = mutableListOf(component)
        }
    }

    /**
     * Provides the instance of a component filtering by the requested type [T].
     * It should be noted that the application component must implement the interface
     * for the requested component in order to work.
     */
    inline fun <reified T : Any> component(): T {
        synchronized(this) {
            return components
                .filterIsInstance<T>()
                .single()
        }
    }

    inline fun <reified T : Any> addOrReplaceComponentIfRequired(component: () -> T): T {
        if (!exists<T>()) {
            addOrReplace(component())
        }
        return ComponentHolder.component()
    }
}