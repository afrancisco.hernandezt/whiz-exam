package com.exam.viapath.di.component

import com.exam.viapath.di.application.InjectableApplication
import dagger.android.AndroidInjector

interface BaseComponent : AndroidInjector<InjectableApplication>