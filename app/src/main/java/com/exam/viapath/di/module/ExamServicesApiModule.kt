package com.exam.viapath.di.module

import com.exam.viapath.data.service.ReceiptService
import com.exam.viapath.di.rest.Microservices
import com.exam.viapath.di.rest.MyExamApi
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit

@Module
class ExamServicesApiModule {

    @MyExamApi
    @Provides
    @Reusable
    fun providesReceiptService(@Microservices retrofit: Retrofit): ReceiptService {
        return retrofit.create(ReceiptService::class.java)
    }
}