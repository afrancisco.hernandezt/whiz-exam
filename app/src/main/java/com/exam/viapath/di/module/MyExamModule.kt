package com.exam.viapath.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module(
    includes = [
        ExamServicesModule::class
    ]
)
class MyExamModule {


    @Provides
    @Reusable
    fun providesGson(): Gson = Gson()
}