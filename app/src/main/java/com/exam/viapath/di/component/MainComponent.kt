package com.exam.viapath.di.component

import com.exam.viapath.di.andvil.AppScope
import com.exam.viapath.di.application.InjectableApplication
import com.exam.viapath.di.module.ExamActivityBuilder
import com.exam.viapath.di.module.MyExamAppModule
import com.exam.viapath.di.module.MyExamModule
import com.exam.viapath.network.NetworkModule
import com.squareup.anvil.annotations.MergeComponent
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@MergeComponent(
    scope = AppScope::class,
    modules = [
        AndroidSupportInjectionModule::class,
        ExamActivityBuilder::class,
        MyExamAppModule::class,
        NetworkModule::class,
        MyExamModule::class
    ]
)
interface MainComponent : BaseComponent, MyExamComponent {

    @Component.Builder
    interface Builder {
        fun build(): MainComponent

        @BindsInstance
        fun application(application: InjectableApplication): Builder
    }
}