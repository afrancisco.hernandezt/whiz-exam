package com.exam.viapath.di.rest

import javax.inject.Qualifier

@Qualifier
annotation class Microservices

@Qualifier
annotation class AuthorizationInterceptors

@Qualifier
annotation class MyExamApi