package com.exam.viapath.di.providers

import com.exam.viapath.di.component.MyExamComponent

interface MyExamComponentProvider {
    fun provideAppComponent(): MyExamComponent
}