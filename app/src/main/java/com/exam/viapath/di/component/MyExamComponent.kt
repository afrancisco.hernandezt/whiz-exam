package com.exam.viapath.di.component

import android.app.Application
import android.content.Context
import com.exam.viapath.data.database.MyDataBaseHelper

interface MyExamComponent {

    fun context(): Context

    fun application(): Application

    fun dataBaseHelper(): MyDataBaseHelper
}