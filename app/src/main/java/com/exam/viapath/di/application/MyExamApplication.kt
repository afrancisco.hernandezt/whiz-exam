package com.exam.viapath.di.application

import com.exam.viapath.di.component.BaseComponent

abstract class MyExamApplication: InjectableApplication() {

    var component:BaseComponent? = null
        protected set

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        var instance: MyExamApplication? = null
            private set
    }
}