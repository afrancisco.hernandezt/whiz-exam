package com.exam.viapath.ui.fragmentsheet

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.exam.viapath.base.dialogs.BaseBottomSheet
import com.exam.viapath.databinding.FragmentSheetMoreInfoReceiptBinding
import com.exam.viapath.ui.model.ReceiptDetailUi
import com.exam.viapath.ui.model.ReceiptUiModel
import com.exam.viapath.utils.fromHtml
import com.exam.viapath.utils.viewBinding

class ConfirmCloseExamSheet(
    private val receipt: ReceiptUiModel,
    private val receiptDetail: ReceiptDetailUi?
) : BaseBottomSheet() {

    private var listener: MoreDetailListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = when {
            context is MoreDetailListener -> context
            parentFragment is MoreDetailListener -> parentFragment as MoreDetailListener
            else -> throw ClassCastException("Must implement MoreDetailListener")
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private val binding: FragmentSheetMoreInfoReceiptBinding by viewBinding {
        FragmentSheetMoreInfoReceiptBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        isCancelable = false
        initListener()
        initUi()
        return binding.root
    }

    private fun initUi() {
        binding.apply {
            textViewTitle.text = receiptDetail?.title
            textViewDescription.text = fromHtml(receiptDetail?.summary)
        }
    }

    private fun initListener() {
        binding.apply {
            buttonMoreInfo.setOnClickListener {
                dismiss()
                receiptDetail?.let {
                    listener?.onMoreDetailClick(it)
                }
            }
            buttonSaveForLatter.setOnClickListener {
                dismiss()
                receiptDetail?.let {
                    listener?.onSaveForLatterClick(receipt, it)
                }
            }
            textViewClose.setOnClickListener{
                dismiss()
            }
        }
    }

    interface MoreDetailListener {
        fun onMoreDetailClick(item: ReceiptDetailUi)
        fun onSaveForLatterClick(
            receipt: ReceiptUiModel,
            receiptDetail: ReceiptDetailUi
        )
    }
}