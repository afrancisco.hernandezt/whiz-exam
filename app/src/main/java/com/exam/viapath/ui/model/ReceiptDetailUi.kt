package com.exam.viapath.ui.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class ReceiptDetailUi(
    val title: String,
    val creditsText: String,
    val urlWebSite: String,
    val summary: String,
    val time: Int,
    val image: String,
    val instructions: String,
    val extendedIngredients: @RawValue List<ExtendedIngredients>,
    val pricePerServing: Float
) : Parcelable {
    data class ExtendedIngredients(
        val aisle: String,
        val nameClean: String,
        val amount: Float,
        val unit: String,
        val original: String
    )
}
