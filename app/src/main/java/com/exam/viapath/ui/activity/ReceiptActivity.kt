package com.exam.viapath.ui.activity

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.NavDirections
import com.exam.viapath.R
import com.exam.viapath.base.activity.BaseFragmentActivity
import com.exam.viapath.ui.fragment.SearchReceiptFragment
import com.exam.viapath.ui.fragment.SearchReceiptFragmentDirections
import com.exam.viapath.ui.model.ReceiptDetailUi
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class ReceiptActivity: BaseFragmentActivity(), HasAndroidInjector,
    SearchReceiptFragment.NavigateListener {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    private fun navigateTo(direction: NavDirections) =
        provideNav().navigate(direction)

    private fun provideNav() = findNavController(R.id.navHostFragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt)
    }

    override fun navigateToDetail(item: ReceiptDetailUi) {
        navigateTo(SearchReceiptFragmentDirections.actionToDetail(item))
    }
}