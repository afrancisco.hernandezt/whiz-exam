package com.exam.viapath.ui.views

import android.view.View
import com.exam.viapath.R
import com.exam.viapath.databinding.ItemViewInstructionBinding
import com.xwray.groupie.viewbinding.BindableItem

class ItemInstructionView(
    private val itemInstruction: String
) : BindableItem<ItemViewInstructionBinding>() {

    override fun bind(viewBinding: ItemViewInstructionBinding, position: Int) {
        viewBinding.textViewItemInstruction.text = itemInstruction
    }

    override fun getLayout() = R.layout.item_view_instruction

    override fun initializeViewBinding(view: View) =
        ItemViewInstructionBinding.bind(view)
}