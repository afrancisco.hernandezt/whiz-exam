package com.exam.viapath.ui.actions

import com.exam.viapath.ui.model.ItemReceiptDB
import com.exam.viapath.ui.model.ListReceiptModel
import com.exam.viapath.ui.model.ReceiptDetailUi
import com.exam.viapath.ui.model.ReceiptUiModel

sealed interface ReceiptAction {
    data class ShowListItems(
        val listItems: ListReceiptModel
    ) : ReceiptAction

    data class ShowShortDetail(
        val receipt: ReceiptUiModel,
        val detailUi: ReceiptDetailUi
    ) : ReceiptAction

    data class ShowStoredData(
        val receiptDetail: List<ItemReceiptDB>
    ) : ReceiptAction

    object HideStoredSection : ReceiptAction
}