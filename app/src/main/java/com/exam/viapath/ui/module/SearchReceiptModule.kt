package com.exam.viapath.ui.module

import com.exam.viapath.scope.FragmentScope
import com.exam.viapath.ui.fragment.DetailReceiptFragment
import com.exam.viapath.ui.fragment.SearchReceiptFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SearchReceiptModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesSearchReceiptFragment(): SearchReceiptFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesDetailReceiptFragment(): DetailReceiptFragment
}