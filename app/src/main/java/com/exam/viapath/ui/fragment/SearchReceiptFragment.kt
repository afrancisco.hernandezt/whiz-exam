package com.exam.viapath.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.exam.viapath.base.fragment.FragmentView
import com.exam.viapath.databinding.FragmentSearchReceiptBinding
import com.exam.viapath.ui.actions.ReceiptAction
import com.exam.viapath.ui.fragmentsheet.ConfirmCloseExamSheet
import com.exam.viapath.ui.model.ItemReceiptDB
import com.exam.viapath.ui.model.ReceiptDetailUi
import com.exam.viapath.ui.model.ReceiptUiModel
import com.exam.viapath.ui.viewmodels.ReceiptViewModel
import com.exam.viapath.ui.views.ItemProductView
import com.exam.viapath.utils.className
import com.exam.viapath.utils.hide
import com.exam.viapath.utils.show
import com.exam.viapath.utils.viewBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class SearchReceiptFragment : FragmentView(), ConfirmCloseExamSheet.MoreDetailListener {

    @Inject
    lateinit var viewModel: ReceiptViewModel

    private val binding: FragmentSearchReceiptBinding by viewBinding {
        FragmentSearchReceiptBinding.inflate(layoutInflater)
    }

    private val groupAdapter = GroupAdapter<GroupieViewHolder>()
    private val groupAdapterStored = GroupAdapter<GroupieViewHolder>()

    private var listener: NavigateListener? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        listener = context as? NavigateListener
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        initUi()
        initRecyclerView()
        getStoredData()
    }

    private fun getStoredData() {
        viewModel.getStoredData()
    }

    private fun initUi() {
        binding.searchViewReceipt.apply {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    query?.let {
                        viewModel.getReceipt(it)
                        getStoredData()
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean = false
            })
        }
    }

    private fun bindViewModel() {
        viewModel.getShowProgress().observe(viewLifecycleOwner, ::showLoading)
        viewModel.getShowError().observe(viewLifecycleOwner, ::showError)
        viewModel.getAction().observe(viewLifecycleOwner, ::handleAction)
    }

    private fun initRecyclerView() {
        binding.recyclerViewReceipt.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = groupAdapter
        }
        binding.recyclerViewStoredReceipt.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = groupAdapterStored
        }
    }

    private fun handleAction(action: ReceiptAction) {
        when (action) {
            ReceiptAction.HideStoredSection -> hideStoredSection()
            is ReceiptAction.ShowStoredData -> showStoredSection(action.receiptDetail)
            is ReceiptAction.ShowListItems -> setUpUi(action.listItems.listItems)
            is ReceiptAction.ShowShortDetail -> showShortDetail(
                action.receipt,
                action.detailUi
            )
        }
    }

    private fun showStoredSection(receiptDetail: List<ItemReceiptDB>) {
        binding.layoutStoredReceipt.show()
        groupAdapterStored.apply {
            clear()
            addAll(
                receiptDetail.map {
                    ItemProductView(it.receipt, it.receiptDetail)
                }
            )
            setOnItemClickListener { item, _ ->
                showShortDetail(
                    (item as ItemProductView).receipt,
                    item.receiptDetail
                )
            }
        }
    }

    private fun hideStoredSection() {
        binding.layoutStoredReceipt.hide()
    }

    private fun showShortDetail(
        receipt: ReceiptUiModel,
        detailUi: ReceiptDetailUi?
    ) {
        ConfirmCloseExamSheet(
            receipt,
            detailUi
        ).show(childFragmentManager, className())
    }

    private fun setUpUi(listItems: List<ReceiptUiModel>) {
        groupAdapter.apply {
            clear()
            addAll(
                listItems.map {
                    ItemProductView(it)
                }
            )
            setOnItemClickListener { item, _ ->
                viewModel.getDetailReceipt((item as ItemProductView).receipt)
            }
        }
    }

    override fun onMoreDetailClick(item: ReceiptDetailUi) {
        listener?.navigateToDetail(item)
    }

    override fun onSaveForLatterClick(
        receipt: ReceiptUiModel,
        receiptDetail: ReceiptDetailUi
    ) {
        viewModel.saveReceiptSearched(receipt, receiptDetail)
    }

    interface NavigateListener {
        fun navigateToDetail(item: ReceiptDetailUi)
    }
}