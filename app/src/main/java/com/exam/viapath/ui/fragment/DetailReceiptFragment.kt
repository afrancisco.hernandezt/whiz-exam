package com.exam.viapath.ui.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.exam.viapath.R
import com.exam.viapath.base.fragment.FragmentView
import com.exam.viapath.databinding.FragmentDetailReceiptBinding
import com.exam.viapath.ui.fragmentsheet.IngredientsSheet
import com.exam.viapath.utils.className
import com.exam.viapath.utils.fromHtml
import com.exam.viapath.utils.viewBinding
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.item_view_single_image.*
import kotlinx.android.synthetic.main.item_view_single_ingredients.textView_title_credits
import kotlinx.android.synthetic.main.item_view_single_instructions.*
import kotlinx.android.synthetic.main.item_view_single_summary.*
import kotlinx.android.synthetic.main.item_view_single_title.*
import kotlinx.android.synthetic.main.item_view_single_title.textView_cost
import kotlinx.android.synthetic.main.item_view_single_title.textView_time

class DetailReceiptFragment : FragmentView() {

    private val args: DetailReceiptFragmentArgs by navArgs()

    private val binding: FragmentDetailReceiptBinding by viewBinding {
        FragmentDetailReceiptBinding.inflate(layoutInflater)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInitUi()
    }

    private fun setInitUi() {
        setTitleUi()
        setImageBackgroundUi()
        setSummaryUi()
        setIngredientsUi()
        setInstructions()
        initListener()
    }

    private fun setTitleUi() {
        textView_receipt_title.text = args.detailReceipt.title
        textView_cost.text =
            "${getString(R.string.receipt_title_cost)} ${args.detailReceipt.pricePerServing}"
        textView_time.text =
            "${getString(R.string.receipt_title_ready_ay)} ${args.detailReceipt.time}"
    }

    private fun setImageBackgroundUi() {
        Picasso.get()
            .load(args.detailReceipt.image)
            .into(imageView_detail)
    }

    private fun setSummaryUi() {
        textView_summary.text = fromHtml(args.detailReceipt.summary)
    }

    private fun setIngredientsUi() {
        textView_title_credits.text = args.detailReceipt.creditsText
    }

    private fun setInstructions() {
        textView_instructions.text = fromHtml(args.detailReceipt.instructions)
    }

    private fun initListener() {
        binding.frameIngredients.setOnClickListener {
            val listItems = args.detailReceipt.extendedIngredients.map {
                it.original
            }
            IngredientsSheet(listItems).show(childFragmentManager, className())
        }

        binding.buttonMoreInfo.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(args.detailReceipt.urlWebSite)
            startActivity(intent)
        }
    }
}