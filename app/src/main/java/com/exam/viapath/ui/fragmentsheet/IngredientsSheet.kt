package com.exam.viapath.ui.fragmentsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.exam.viapath.base.dialogs.BaseBottomSheet
import com.exam.viapath.databinding.FragmentSheetMoreIngredientsBinding
import com.exam.viapath.ui.views.ItemInstructionView
import com.exam.viapath.utils.viewBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder

class IngredientsSheet(
    private val itemList: List<String>
): BaseBottomSheet() {

    private val binding: FragmentSheetMoreIngredientsBinding by viewBinding {
        FragmentSheetMoreIngredientsBinding.inflate(layoutInflater)
    }

    private val groupAdapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        isCancelable = true
        initRecyclerView()
        initListener()
        setInstructionItems()
        return binding.root
    }

    private fun initRecyclerView() {
        binding.recyclerViewInstructionList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = groupAdapter
        }
    }

    private fun setInstructionItems() {
        groupAdapter.addAll(
            itemList.map {
                ItemInstructionView(it)
            }
        )
    }

    private fun initListener() {
        binding.textViewClose.setOnClickListener {
            dismiss()
        }
    }
}