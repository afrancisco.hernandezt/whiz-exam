package com.exam.viapath.ui.viewmodels

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.exam.viapath.R
import com.exam.viapath.base.viewmodel.BaseViewModel
import com.exam.viapath.data.database.ExamContract
import com.exam.viapath.data.database.MyDataBaseHelper
import com.exam.viapath.data.repository.ReceiptRepository
import com.exam.viapath.ui.actions.ReceiptAction
import com.exam.viapath.ui.model.ItemReceiptDB
import com.exam.viapath.ui.model.ReceiptDetailUi
import com.exam.viapath.ui.model.ReceiptUiModel
import com.google.gson.Gson
import java.lang.Exception
import javax.inject.Inject

class ReceiptViewModel @Inject constructor(
    private val repository: ReceiptRepository,
    private val dataBase: MyDataBaseHelper
) : BaseViewModel() {

    private val action = MutableLiveData<ReceiptAction>()
    fun getAction(): LiveData<ReceiptAction> = action

    fun getReceipt(query: String) {
        disposable.add(
            repository.getListReceipt(query)
                .doOnSubscribe { showProgress.value = true }
                .doFinally { showProgress.value = false }
                .subscribe({
                    if (it != null) {
                        action.value = ReceiptAction.ShowListItems(it)
                    } else {
                        showError.value = R.string.app_error_service
                    }
                }, {
                    showError.value = R.string.app_error_service
                })
        )
    }

    fun getDetailReceipt(item: ReceiptUiModel) {
        disposable.add(
            repository.getReceiptDetail(item.id)
                .doOnSubscribe { showProgress.value = true }
                .doFinally { showProgress.value = false }
                .subscribe({
                    if (it != null) {
                        action.value = ReceiptAction.ShowShortDetail(item, it)
                    } else {
                        showError.value = R.string.app_error_service
                    }
                }, {
                    showError.value = R.string.app_error_service
                })
        )
    }

    fun saveReceiptSearched(receipt: ReceiptUiModel, receiptDetail: ReceiptDetailUi) {
        dataBase.saveReceipt(ItemReceiptDB(receipt, receiptDetail))
        getStoredData()
    }

    @SuppressLint("Range")
    fun getStoredData() {
        try {
            val cursor = dataBase.getAllItems()
            val listReceipt = mutableListOf<ItemReceiptDB>()

            while (cursor.moveToNext()) {
                val receipt = cursor.getString(cursor.getColumnIndex(ExamContract.RECEIPT_SEARCHED))
                val receiptDetail =
                    cursor.getString(cursor.getColumnIndex(ExamContract.RECEIPT_DETAIL))

                listReceipt.add(
                    ItemReceiptDB(
                        Gson().fromJson(receipt, ReceiptUiModel::class.java),
                        Gson().fromJson(receiptDetail, ReceiptDetailUi::class.java)
                    )
                )
            }
            action.postValue(
                if (listReceipt.isNotEmpty()) {
                    ReceiptAction.ShowStoredData(listReceipt)
                } else {
                    ReceiptAction.HideStoredSection
                }
            )
        } catch (ex: Exception) {
            action.postValue(ReceiptAction.HideStoredSection)
        }
    }
}