package com.exam.viapath.ui.views

import android.view.View
import com.exam.viapath.R
import com.exam.viapath.databinding.ViewItemProductBinding
import com.exam.viapath.ui.model.ReceiptDetailUi
import com.exam.viapath.ui.model.ReceiptUiModel
import com.squareup.picasso.Picasso
import com.xwray.groupie.viewbinding.BindableItem

class ItemProductView(
    val receipt: ReceiptUiModel,
    val receiptDetail: ReceiptDetailUi? = null
) : BindableItem<ViewItemProductBinding>() {

    override fun getLayout(): Int = R.layout.view_item_product

    override fun bind(viewBinding: ViewItemProductBinding, position: Int) {

        viewBinding.run {
            textViewCardTitle.text = receipt.title
        }

        Picasso.get()
            .load(receipt.image)
            .into(viewBinding.imageViewProduct)
    }

    override fun initializeViewBinding(view: View): ViewItemProductBinding =
        ViewItemProductBinding.bind(view)
}