package com.exam.viapath.ui.model

import android.content.ContentValues
import com.exam.viapath.data.database.ExamContract
import com.google.gson.Gson

data class ListReceiptModel(
    val listItems: List<ReceiptUiModel>
)

data class ReceiptUiModel(
    val title: String,
    val image: String,
    val id: Int
)

data class ItemReceiptDB(
    val receipt: ReceiptUiModel,
    val receiptDetail: ReceiptDetailUi
)

fun ItemReceiptDB.toContentValues(): ContentValues {
    val gsonReceipt = Gson().toJson(receipt)
    val gsonReceiptDetail = Gson().toJson(receiptDetail)
    return ContentValues().apply {
        put(ExamContract.RECEIPT_SEARCHED, gsonReceipt)
        put(ExamContract.RECEIPT_DETAIL, gsonReceiptDetail)
    }
}