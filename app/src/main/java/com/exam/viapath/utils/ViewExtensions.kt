package com.exam.viapath.utils

import android.annotation.SuppressLint
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import androidx.core.view.updateMargins

fun Any.className(): String = this::class.java.name

@SuppressLint("NewApi")
fun fromHtml(source: String?): Spanned {
    return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
}

fun View.removeFromParent() {
    this.parent?.let {
        (it as ViewGroup).removeView(this)
    }
}

fun View.show(show: Boolean) {
    if (show) {
        show()
    } else {
        hide()
    }
}

fun View.hide(hide: Boolean) {
    if (hide) {
        visibility = View.INVISIBLE
    } else {
        show()
    }
}

fun View.showOrDisappear(show: Boolean) {
    if (show) {
        show()
    } else {
        disappear()
    }
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.disappear() {
    this.visibility = View.INVISIBLE
}

fun View.updateHorizontalMargins(start: Int, end: Int = start) {
    (layoutParams as ViewGroup.MarginLayoutParams).updateMargins(
        left = start,
        right = end
    )
}

fun View.updateVerticalMargins(top: Int, bottom: Int = top) {
    (layoutParams as ViewGroup.MarginLayoutParams).updateMargins(
        top = top,
        bottom = bottom
    )
}

fun View.updateBottomMargin(bottom: Int) =
    updateLayoutParams<ViewGroup.MarginLayoutParams> {
        bottomMargin = bottom
    }

fun View.updateTopMargin(top: Int) =
    updateLayoutParams<ViewGroup.MarginLayoutParams> {
        topMargin = top
    }

fun View.updateHeight(newHeight: Int) {
    layoutParams.height = newHeight
}

fun View.getColor(@ColorRes color: Int) = ContextCompat.getColor(context, color)

fun View.getDrawable(@DrawableRes drawable: Int) = ContextCompat.getDrawable(context, drawable)

fun View.setBackgroundColorResource(@ColorRes color: Int) = setBackgroundColor(getColor(color))