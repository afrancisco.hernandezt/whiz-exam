package com.exam.viapath.base.mapper

interface BaseMapper<I, O> {

    fun map(input: I): O
}