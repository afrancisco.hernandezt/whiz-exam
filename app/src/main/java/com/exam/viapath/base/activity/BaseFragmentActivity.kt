package com.exam.viapath.base.activity

import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.exam.viapath.R
import com.exam.viapath.base.interfaces.AppAndroidProvider
import com.exam.viapath.base.views.AppLoadingView
import com.exam.viapath.utils.removeFromParent

open class BaseFragmentActivity : AppCompatActivity(), AppAndroidProvider {

    private val loadingView by lazy { AppLoadingView(this) }

    override fun showError(message: String, iconErrorAlert: Int?) {
        TODO("Not yet implemented")
    }

    override fun showError(message: String) {
        dismissProgressDialog()
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showError(messageRes: Int) {
        dismissProgressDialog()
        Toast.makeText(this, messageRes, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(message: String, iconSuccessAlert: Int?) {
        dismissProgressDialog()
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(message: String) {
        dismissProgressDialog()
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(@StringRes messageRes: Int) = showMessage(getString(messageRes))

    override fun showLoading(show: Boolean) {
        if (show) showProgressDialog() else dismissProgressDialog()
    }

    override fun showProgressDialog() = showProgressDialog(R.string.app_copy_loading)

    override fun dismissProgressDialog() = loadingView.removeFromParent()

    override fun backToolbar() = onBackPressed()

    private fun showProgressDialog(@StringRes idMessage: Int) =
        showProgressDialog(getString(idMessage))

    private fun showProgressDialog(message: String) {
        loadingView.setMessage(message)
        if (loadingView.parent == null) {
            addContentView(
                loadingView,
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            )
        }
    }
}