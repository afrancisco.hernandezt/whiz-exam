package com.exam.viapath.base.fragment

import android.content.Intent
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.exam.viapath.base.activity.BaseFragmentActivity
import com.exam.viapath.base.interfaces.AppAndroidProvider

abstract class FragmentView : Fragment(), AppAndroidProvider {

    val baseActivity by lazy {
        requireActivity() as BaseFragmentActivity
    }

    override fun showError(message: String, iconErrorAlert: Int?) {
        baseActivity.showError(message, iconErrorAlert)
    }

    override fun showError(@StringRes messageRes: Int) {
        baseActivity.showError(messageRes)
    }

    override fun showError(message: String) {
        baseActivity.showError(message)
    }

    override fun showMessage(message: String, iconSuccessAlert: Int?) {
        baseActivity.showMessage(message, iconSuccessAlert)
    }

    override fun showMessage(@StringRes messageRes: Int) {
        baseActivity.showMessage(messageRes)
    }

    override fun showMessage(message: String) {
        baseActivity.showMessage(message)
    }

    override fun showLoading(show: Boolean) {
        baseActivity.showLoading(show)
    }

    override fun dismissProgressDialog() {
        baseActivity.dismissProgressDialog()
    }

    override fun backToolbar() {
        baseActivity.backToolbar()
    }

    override fun startActivity(intent: Intent) {
        baseActivity.startActivity(intent)
    }

    override fun showProgressDialog() {
        baseActivity.showProgressDialog()
    }

    open fun onBackPressed() = Unit

    open fun initToolbar() = Unit
}