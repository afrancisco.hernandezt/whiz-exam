package com.exam.viapath.base.interfaces

import android.content.Intent
import androidx.annotation.StringRes

interface AppAndroidProvider {

    fun showError(message: String, iconErrorAlert: Int? = null)

    fun showError(message: String)

    fun showError(@StringRes messageRes: Int)

    fun showMessage(message: String, iconSuccessAlert: Int? = null)

    fun showMessage(message: String)

    fun showMessage(@StringRes messageRes: Int)

    fun showLoading(show: Boolean)

    fun showProgressDialog()

    fun dismissProgressDialog()

    fun startActivity(intent: Intent)

    fun startActivityForResult(intent: Intent, requestCode: Int)

    fun backToolbar()
}