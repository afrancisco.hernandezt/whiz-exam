package com.exam.viapath.data.database

import android.provider.BaseColumns

class ExamContract : BaseColumns {
    companion object {
        const val RECEIPT_SEARCHED = "receipt_searched"
        const val RECEIPT_DETAIL = "receipt_detail"

        const val RECEIPT = "search_receipt"
    }
}