package com.exam.viapath.data.mapper

import com.exam.viapath.base.mapper.BaseMapper
import com.exam.viapath.domain.models.DetailReceiptResponse
import com.exam.viapath.ui.model.ReceiptDetailUi
import javax.inject.Inject

class DetailReceiptMapper @Inject constructor() :
    BaseMapper<DetailReceiptResponse, ReceiptDetailUi> {

    override fun map(input: DetailReceiptResponse): ReceiptDetailUi {
        return ReceiptDetailUi(
            title = input.title.orEmpty(),
            creditsText = input.creditsText.orEmpty(),
            urlWebSite = input.urlWebSite.orEmpty(),
            summary = input.summary.orEmpty(),
            time = input.time ?: 0,
            image = input.image.orEmpty(),
            instructions = input.instructions.orEmpty(),
            extendedIngredients = mapListInstruction(input.extendedIngredients),
            pricePerServing = input.pricePerServing ?: 0f
        )
    }

    private fun mapListInstruction(
        extendedIngredients: List<DetailReceiptResponse.ExtendedIngredients>?
    ): List<ReceiptDetailUi.ExtendedIngredients> {
        return extendedIngredients?.map {
            ReceiptDetailUi.ExtendedIngredients(
                aisle = it.aisle.orEmpty(),
                nameClean = it.nameClean.orEmpty(),
                amount = it.amount ?: 0f,
                unit = it.unit.orEmpty(),
                original = it.original.orEmpty()
            )
        } ?: emptyList()
    }
}