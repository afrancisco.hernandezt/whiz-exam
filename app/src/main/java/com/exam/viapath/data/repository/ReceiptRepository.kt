package com.exam.viapath.data.repository

import com.exam.viapath.data.mapper.DetailReceiptMapper
import com.exam.viapath.data.mapper.ReceiptMapper
import com.exam.viapath.data.service.ReceiptService
import com.exam.viapath.di.rest.MyExamApi
import com.exam.viapath.ui.model.ListReceiptModel
import com.exam.viapath.ui.model.ReceiptDetailUi
import com.exam.viapath.utils.applySchedulers
import dagger.Reusable
import io.reactivex.Single
import javax.inject.Inject

private const val BASE_API_KEY = "a19fb7edd067434b99e0b91b7010e35e"

@Reusable
class ReceiptRepository @Inject constructor(
    @MyExamApi private val receiptService: ReceiptService,
    private val mapper: ReceiptMapper,
    private val detailReceiptMapper: DetailReceiptMapper
) {
    fun getListReceipt(query: String): Single<ListReceiptModel> {
        return receiptService.searchReceipt(
            query,
            BASE_API_KEY
        ).map {
            mapper.map(it)
        }.applySchedulers()
    }

    fun getReceiptDetail(id: Int): Single<ReceiptDetailUi> {
        return receiptService.getDetailReceipt(
            id,
            BASE_API_KEY
        ).map {
            detailReceiptMapper.map(it)
        }.applySchedulers()
    }
}