package com.exam.viapath.data.service

import com.exam.viapath.domain.models.DetailReceiptResponse
import com.exam.viapath.domain.models.ReceiptResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ReceiptService {

    @GET("recipes/complexSearch")
    fun searchReceipt(
        @Query("query") receipt: String,
        @Query("apiKey") apiKey: String
    ): Single<ReceiptResponse>

    @GET("recipes/{idReceipt}/information")
    fun getDetailReceipt(
        @Path("idReceipt") id: Int,
        @Query("apiKey") apiKey: String
    ): Single<DetailReceiptResponse>
}