package com.exam.viapath.data.mapper

import com.exam.viapath.base.mapper.BaseMapper
import com.exam.viapath.domain.models.ReceiptResponse
import com.exam.viapath.ui.model.ListReceiptModel
import com.exam.viapath.ui.model.ReceiptUiModel
import javax.inject.Inject

class ReceiptMapper @Inject constructor() : BaseMapper<ReceiptResponse, ListReceiptModel> {

    override fun map(input: ReceiptResponse): ListReceiptModel {
        val list = input.listResult?.map {
            ReceiptUiModel(
                title = it.title.orEmpty(),
                image = it.image.orEmpty(),
                id = it.id ?: 0
            )
        } ?: emptyList()

        return ListReceiptModel(list.filter { it.id != 0 })
    }
}