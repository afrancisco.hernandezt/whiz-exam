package com.exam.viapath.data.database

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns._ID
import com.exam.viapath.data.database.ExamContract.Companion.RECEIPT
import com.exam.viapath.data.database.ExamContract.Companion.RECEIPT_DETAIL
import com.exam.viapath.data.database.ExamContract.Companion.RECEIPT_SEARCHED
import com.exam.viapath.ui.model.ItemReceiptDB
import com.exam.viapath.ui.model.ReceiptUiModel
import com.exam.viapath.ui.model.toContentValues

class MyDataBaseHelper(
    val context: Context
): SQLiteOpenHelper(
    context,
    EXAM_DB,
    null,
    VERSION
) {

    override fun onCreate(db: SQLiteDatabase) {
        val receipt = "CREATE TABLE $RECEIPT (" +
            "$_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
            "$RECEIPT_SEARCHED TEXT," +
            "$RECEIPT_DETAIL TEXT)"

        db.execSQL(receipt)
    }

    fun saveReceipt(receipt: ItemReceiptDB) {
        writableDatabase.insert(
            RECEIPT, null, receipt.toContentValues()
        )
    }

    fun getAllItems() : Cursor {
        return readableDatabase
            .query(
                RECEIPT,
                null,
                null,
                null,
                null,
                null,
                null
            )
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) = Unit

    companion object {
        const val VERSION = 1
        const val EXAM_DB = "Exam.db"
    }
}